﻿## WAVGAT-DRIVERS

### Para windows
<p>1.Primero descomprimimos descargamos y descomprimimos el repositorio.</p>

<p>2.luego copiamos las 3 carpetas:  hardware,libraries,sketches.</p>

<p>3.luego abrimos la siguiente ubicación  <b>C:\Users\tuusurio\Documents\Arduino</b> , pegamos las tres carpetas, si pide confirmación le decimos que sí.</p>


### Systemas Operativos Linux

<p>Una vez instalado el compilador de Arduino la ejecutamos como root  para que se cree la carpeta Arduino, luego de esto procedemos a cerrarlo, si deseas hacerlo con tu usuario y no con el root debes tener todos los privilegios requeridos</p>
<p>Luego en el directorio /root/Arduino y pegamos las tres carpetas del archivo descomprimimos si solicita confirmacion le damos que sí</p>

### Ejecucion sin  ser root
<p> si deseas ejecutarlo como usuario normal puedes en linux puedes darle suficiente permisos a tu usaurio debes tener en cuenta que para que esta accion se pueda realizar debes terner altos privilegios de administrador</p>

<pre>sudo usermod -a -G uucp <b>tu-usuario</b> </pre>

ahora solo reinicia tu maquina, tambien debes tener en cuenta que los drivers estaran ubicados en la home de tu usuario normal en la capeta<b> ~/Arduino</b> en lugar de <b>/root/Arduino</b>


### Para finalizar 

<p>4.Conectamos nuestra placa WAVGAT y abrimos el compilador de arduino(si es linux se debe ejecutar como root), luego seleccionamos herramientas, placa y seleccionamos nuestra placa  WAVGAT UNO R3 Y luego elegimos el puerto serial.</p>


### Posibles Problemas
<p>Si tiene algún problema con la carga del proyecto en la placa arduino o al momento de verificarlo seleccione Herramientas >> Placa >> Gestor de Tarjetas luego instale Arduino AVR Boards version 1.6.20</p>

